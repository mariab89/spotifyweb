# Proyecto: SpotifyWeb

Permite gestionar la api de Spotify

### Pre-requisitos del servidor 📋

```
Apache >= 2.4
php >= 8.0.13
composer
Symfony 6
```

### Instalación 🔧
```
Debe contar con una cuenta de spotify developer; en caso contrario, puede usar la cuenta predefinida en este proyecto en el archivo services.yaml

```
```
    Clone el repositorio
    Diríjase a la carpeta del proyecto con: cd ./spotifyweb
    Ejecutar en la cónsola composer install
    Ejecutar en la cónsola symfony server:start
    Ingresar en el explorador localhost:8000 ó 127.0.0.1:8000
    Listo
```
## Credenciales Spotify
```
    User: mdesarrollo2022@gmail.com
    Clave: .desarrollo_2022.
```

## Arquitectura 🛠️

* [PHP]
* [APACHE2]
* [Composer]
* [Symfony]
* [Bootstrap]
* [jQuery]

## Desarrollado por:
María Beatriz Rondón de Lockwood

## Licencia 📄

Este proyecto está bajo la Licencia GNU General Public License
