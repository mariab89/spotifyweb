$(document).ready(function() {
    // Initialization
    $('.email').val('');

    // Datatable Artist
    $('#songs').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
    });

    // Users subscription
    $(".subscribe").click(function() {
        let email = $('.email').val();

        if(email != '') {

            let validate = /([A-Z0-9a-z_-][^@])+?@[^$#<>?]+?\.[\w]{2,4}/.test(email);

            if(validate) {
                swal("Proceso Exitoso", "Disfruta de la últimos lanzamientos en tu bandeja de entrada");
            }
        }
    });
} );