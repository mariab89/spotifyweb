<?php
/*
    * Controller: SpotifyApiService
    * Description: Servicio para consumir la api de Spotify
    * Developer: María Rondón
    * Email: rondonmariab@gmail.com
*/

namespace App\Service;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class SpotifyApiService
{
    private $client;
    private $params;

    public function __construct(ParameterBagInterface $params)
    {
        $this->params = $params;
        $this->client = new CurlHttpClient();
    }

    public function getToken($tokenAutorizacion)
    {
        try {
            // Realizo la petición con los parámetros necesarias y envío las cabeceras para solicitar el token de sesión (segundo token)
            $response = $this->client->request('POST', $this->params->get('SPOTIFY_API_URL').$this->params->get('SPOTIFY_API_TOKEN'), [
                'headers' => [
                    'Authorization' => 'basic '.base64_encode($this->params->get('CLIENT_ID').':'.$this->params->get('CLIENT_SECRET')),
                    'Content-Type' => 'application/x-www-form-urlencoded',
                ],
                'query' => [
                    'grant_type' => 'authorization_code',
                    'code' => $tokenAutorizacion,
                    'redirect_uri' => $this->params->get('REDIRECT_URI'),
                ]
            ]);
        } catch (\Exception $e) {
            // Si no se obtiene respuesta u ocurre un error en la solicitud muestro el error log de la petición
            error_log($e->getMessage());
        }
        return json_decode($response->getContent());
    }

    public function buildRequest($url)
    {
        $parametros = [
            'response_type' => 'code',
            'client_id'     => $this->params->get('CLIENT_ID'),
            'scope'         => $this->params->get('SPOTIFY_API_SCOPE'),
            'redirect_uri'  => $this->params->get('REDIRECT_URI')
        ];

        // Completo la url con los parámetros necesarios para solicitar autorización
        $url .= '?'.http_build_query($parametros);

        return $url;
    }

    public function getApiRequest($token, $url)
    {
        try {
            $response = $this->client->request('GET', $this->params->get('URL_BASE').$url, [
                'headers' => [
                    'Authorization' => 'Bearer '.$token,
                    'Content-Type' => 'application/json',
                    'Accept' => 'application/json',
                ]
            ]);
        } catch (\Exception $e) {
            error_log($e->getMessage());
        }
       
        return json_decode($response->getContent());
    }
}