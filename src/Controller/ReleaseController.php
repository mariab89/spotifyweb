<?php
/*
    * Controller: ReleaseController
    * Description: Gestiona los ultimos lanzamientos de los artistas
    * Developer: María Rondón
    * Email: rondonmariab@gmail.com
*/

namespace App\Controller;

use App\Service\SpotifyApiService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ReleaseController extends AbstractController
{
    private $api;

    public function __construct(ParameterBagInterface $params, SpotifyApiService $SpotifyApiService)
    {
        $this->params = $params;
        $this->api = $SpotifyApiService;
    }

    // Realiza la primera petición para solicitar el token de autorización
    #[Route('/', name: 'login')]
    public function index(Request $request)
    {   
        // Construyo la url con el empoint de autorización
        $url = $this->params->get('SPOTIFY_API_URL') . $this->params->get('SPOTIFY_API_AUTORIZAR');

        // Consulto si el token de sesión se encuentra en la cookie del explorador
        $token = $request->cookies->get($this->params->get('SPOTIFY_NAME_COOKIE'));

        // Si no existe o el navegador lo eliminó, debo solicitarlo de nuevo
        if(!$token) {

            $url = $this->api->buildRequest($url);

            // Redirige a la url de Spotify para que el usuario inicie sesión (externo)
            return $this->redirect($url);
        } 
        
        // Si existe la cookie en el explorador, procedemos a solicitar los últimos lanzamientos
        $releaseConsumer = $this->api->getApiRequest($token, $this->params->get('URL_RELEASE'));

        return $this->render('base.html.twig', ['data' => $releaseConsumer->albums->items]);
    }

    // Segunda solicitud donde se obtiene el token para el consumo
    #[Route('/lanzamientos', name: 'lanzamientos')]
    public function callback(Request $request): Response
    {
        // Se obtiene el token para poder realizar el consumo a la Api de Spotify
        $respuesta = $this->api->getToken($request->query->get('code'));

        // Consumo la api para obtener la data de ultimos lanzamientos
        $releaseConsumer = $this->api->getApiRequest($respuesta->access_token, $this->params->get('URL_RELEASE'));

        $response = $this->render('base.html.twig', ['data' => $releaseConsumer->albums->items]);

        // El token se almacena en una cookie en el explorador
        $response->headers->setCookie(new Cookie($this->params->get('SPOTIFY_NAME_COOKIE'), $respuesta->access_token, strtotime('1 minute'), '/', 'localhost', true, true));

        return $response;
    }
}