<?php
/*
    * Controller: ArtistController
    * Description: Gestiona el catalogo de canciones de un artista en específico
    * Developer: María Rondón
    * Email: rondonmariab@gmail.com
*/

namespace App\Controller;

use App\Service\SpotifyApiService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ArtistController extends AbstractController
{
    private $api;

    public function __construct(ParameterBagInterface $params, SpotifyApiService $SpotifyApiService)
    {
        $this->params = $params;
        $this->api = $SpotifyApiService;
    }

    // Obtengo el artista a consultar
    #[Route('/artista/{id}', name: 'artista')]
    public function getArtist($id, Request $request): Response
    {
        $url = $this->params->get('SPOTIFY_API_URL') . $this->params->get('SPOTIFY_API_AUTORIZAR');

        $token = $request->cookies->get($this->params->get('SPOTIFY_NAME_COOKIE'));

        // Si no existe o el navegador lo eliminó, debo solicitarlo de nuevo
        if(!$token) {

            $url = $this->api->buildRequest($url);
            // Redirige a la url de Spotify para que el usuario inicie sesión (externo)
            return $this->redirect($url);
        } 
     
        // Solicito la información del artista al servicio
        $artistConsumer = $this->api->getApiRequest($token, $this->params->get('URL_ARTIST').$id);
      
        // Solicito las canciones top del artista consultado
        $songs = $this->api->getApiRequest($token, $this->params->get('URL_ARTIST').$id.$this->params->get('URL_TRACK'));

        return $this->render('pages/artist.html.twig', ['songs' => $songs->tracks, 'data' => $artistConsumer]);
    }
}