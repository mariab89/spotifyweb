<?php
/*
    * Controller: GeneralController
    * Description: Gestion de páginas informativas
    * Developer: María Rondón
    * Email: rondonmariab@gmail.com
*/

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GeneralController extends AbstractController
{

    // Muestra una breve descripción sobre el propósito de la página
    #[Route('/nosotros', name: 'nosotros')]
    public function showAbout(): Response
    {   
        return $this->render('pages/about.html.twig');
    }

    // Muestra la página de contacto
    #[Route('/contacto', name: 'contacto')]
    public function showContact(): Response
    {   
        return $this->render('pages/contact.html.twig');
    }
}
